import * as React from "react";
import { ITemplate } from "./Template.interface";
abstract class Component<P = any, S = any, SS = any> extends React.Component<P, S, SS> {
    //Template
    abstract template: ITemplate
    /**
     * View
     */
    abstract view(): any
    /**
     * Render
     */
    render(): React.ReactNode {
        const formattedData = this.view();
        return this.template(formattedData, this);
    }
}
//Export
export { Component }