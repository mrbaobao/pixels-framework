import * as React from "react";
abstract class Page extends React.Component<any, any>{
    //Render
    render(): React.ReactNode {
        return (
            <>
                {this.build()}
            </>
        )
    }
    //Build
    abstract build(): React.ReactNode;
}
//Export
export { Page }