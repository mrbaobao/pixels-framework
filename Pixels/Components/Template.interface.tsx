import { ReactNode } from "react"
/**
 * Template
 */
type ITemplate = (params: any, self: Object) => ReactNode
//Export
export type { ITemplate }