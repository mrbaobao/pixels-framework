import * as React from "react"

/**
 * If
 */
interface IfProps {
    condition: boolean
}
class If extends React.Component<IfProps, any> {
    //Render
    render() {
        const { condition } = this.props
        //Then
        if (condition) {
            return this.props.children
        }
        //Empty
        return null
    }
}
//Export
export { If }