import * as React from "react"


/**
 * For
 */
interface ForProps {
    from: number,
    to: number,
    row: (i: number) => React.ReactElement
}
class For extends React.Component<ForProps, any> {
    //Render
    render() {
        const { from, to, row } = this.props
        const elms: any[] = []
        for (let i = from; i <= to; i++) {
            elms.push(row(i))
        }
        return elms
    }
}
/**
 * List
 */
 interface ListProps {
    list: any[],
    row: (item: any, i: number) => React.ReactElement
}
class List extends React.Component<ListProps, any> {
    //Render
    render() {
        const { list, row } = this.props
        const elms: any[] = []
        const length = list.length
        for (let i = 0; i < length; i++) {
            elms.push(row(list[i], i))
        }
        return elms
    }
}
//Export
export { List, For }