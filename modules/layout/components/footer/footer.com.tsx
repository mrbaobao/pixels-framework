import { Component } from "../../../../Pixels/Components/Component"
import { FooterTpl } from "./footer.tpl"

class Footer extends Component {
    //Template
    template = FooterTpl
    ///Constructor
    constructor(props: any) {
        super(props)
        //State
        this.state = {}
    }
    /**
     * View
     */
    view = () => {
        const poweredBy = "Powered by mrbaobao"
        return { poweredBy }
    }
}
//Export
export { Footer }