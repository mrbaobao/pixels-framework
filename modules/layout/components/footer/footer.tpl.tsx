import { ITemplate } from "../../../../Pixels/Components/Template.interface";
import { FooterCss } from "./footer.css";

const FooterTpl: ITemplate = ({ poweredBy }, self: any) => {
    return (
        <footer>
            <a
                href="https://medium.com/@mrbaobao/about"
                target="_blank"
                rel="noopener noreferrer"
            >
                {poweredBy}
            </a>
            {/* CSS */}
            <FooterCss />
        </footer>
    );
}
//Export
export { FooterTpl }