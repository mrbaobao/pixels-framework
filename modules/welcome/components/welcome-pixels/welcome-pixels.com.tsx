import { Component } from "../../../../Pixels/Components/Component"
import { WelcomePixlesTpl } from "./welcome-pixels.tpl"

class WelcomePixles extends Component {
    //Template
    template = WelcomePixlesTpl
    ///Constructor
    constructor(props: any) {
        super(props)
        //State
        this.state = {}
    }
    /**
     * View
     */
    view = () => {
        const pixels = "The Pixels"
        return { pixels }
    }
}
//Export
export { WelcomePixles }