const WelcomePixlesCss = () => <style jsx global>{`
    .title a {
        color: #0070f3;
        text-decoration: none;
    }

    .title a:hover,
    .title a:focus,
    .title a:active {
        text-decoration: underline;
    }

    .title {
        margin: 0;
        line-height: 1.15;
        font-size: 4rem;
    }

    .title,
    .description {
        text-align: center;
    }

    .description {
        margin: 4rem 0;
        line-height: 1.5;
        font-size: 1.5rem;
    }
    summary {
        margin: 1rem;
        text-align: left;
    }
    summary ul {
        list-style-type: auto;
    }
    summary h2 {
        line-height: 2rem;
    }
`}</style>
//Export
export { WelcomePixlesCss }
