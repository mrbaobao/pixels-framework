import { ITemplate } from "../../../../Pixels/Components/Template.interface";
import { WelcomePixlesCss } from "./welcome-pixels.css";

const WelcomePixlesTpl: ITemplate = ({ pixels }, self: any) => {
    return (
        <>
            <h1 className="title">
                Welcome to <a href="https://gitlab.com/mrbaobao/pixels-framework">{pixels}!</a>
            </h1>
            <p>
                {pixels} is a Javascript framework based on Nextjs/Reactjs.
            </p>
            <summary>
                <h2>{pixels} features:</h2>
                <ul>
                    <li>Reactjs features</li>
                    <li>SSR - Server side rendering from Nextjs</li>
                    <li>Typescript</li>
                    <li>Templates separated with Logical codes</li>
                    <li>and more...</li>
                </ul>
            </summary>
            {/* CSS */}
            <WelcomePixlesCss />
        </>
    );
}
//Export
export { WelcomePixlesTpl }