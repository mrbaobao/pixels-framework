import '../styles/reset.css'
import '../styles/globals.css'
import type { AppProps } from 'next/app'

function PixelsApp({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}
//Exports
export default PixelsApp
