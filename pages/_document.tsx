import Document, { Head, Html, Main, NextScript } from 'next/document'

class PixelsDocument extends Document {
    render() {
        return (
            <Html>
                <Head />
                {/* Body */}
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}
//Export
export default PixelsDocument