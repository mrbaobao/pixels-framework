import Head from 'next/head'
import { Footer } from '../modules/layout/components/footer/footer.com'
import { WelcomePixles } from '../modules/welcome/components/welcome-pixels/welcome-pixels.com'
import { Page } from '../Pixels/Components/Page'
import styles from '../styles/Home.module.css'

class Home extends Page {
  /**
   * Build layout
   */
  build() {
    return (
      <div className={styles.container}>
        <Head>
          <title>Pixels framework with nextjs/reactjs</title>
          <meta name="description" content="Pixels is a framework with nextjs/reactjs" />
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main className={styles.main}>
          <WelcomePixles />
        </main>

        {/* Footer */}
        <Footer />
      </div>
    )
  }
}

export default Home
